package seven;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import main.Base;

public class Seven extends Base {

    Map<Long, Long> hands;

    public long execute() throws FileNotFoundException {
        initialize();

        while (sc.hasNextLine()) {
            addHand(sc.nextLine());
        }

        long sum = 0;
        int i = 1;
        for (Long value : hands.values()) {
            sum += value * i;
            i++;
        }

        return sum;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("7");

        hands = new TreeMap<>();
    }

    private void addHand(String line) {
        hands.put(
            computeValue(line.split(" ")[0]),
            Long.parseLong(line.split(" ")[1]));
    }

    private Long computeValue(String hand) {
        HashMap<Character, Integer> cards = new HashMap<>();
        long value = 0L;
        int length = hand.length();
        for (int i = 0; i < length; i++) {
            char c = hand.charAt(i);
            Integer currentNumber = cards.get(c);
            cards.put(c, currentNumber == null ? 1 : currentNumber + 1);
            value += getCardValuePartTwo(c) * (long) Math.pow(13, (double) length - i - 1);
        }
        value += getTypePartTwo(cards) * (long) Math.pow(13, length);
        return value;
    }

    private Long getCardValuePartOne(char c) {
        switch (c) {
            case 'A':
                return 12L;
            case 'K':
                return 11L;
            case 'Q':
                return 10L;
            case 'J':
                return 9L;
            case 'T':
                return 8L;
            default:
                return Character.getNumericValue(c) - 2L;
        }
    }

    private Long getCardValuePartTwo(char c) {
        switch (c) {
            case 'A':
                return 12L;
            case 'K':
                return 11L;
            case 'Q':
                return 10L;
            case 'T':
                return 9L;
            case 'J':
                return 0L;
            default:
                return Character.getNumericValue(c) - 1L;
        }
    }

    private Long getTypePartOne(Map<Character, Integer> cards) {
        int maxNb = 0;
        for (Integer nb : cards.values()) {
            if (nb > maxNb) {
                maxNb = nb;
            }
        }

        switch (maxNb) {
            case 5:
                return 6L;
            case 4:
                return 5L;
            case 3:
                return cards.size() == 2 ? 4L : 3L;
            case 2:
                return cards.size() == 3 ? 2L : 1L;
            default:
                return 0L;
        }
    } 

    private Long getTypePartTwo(Map<Character, Integer> cards) {
        int maxNb = 0;
        int nbJ = 0;
        for (Map.Entry<Character, Integer> nb : cards.entrySet()) {
            if (nb.getKey().equals('J')) {
                nbJ = nb.getValue();
            } else if (nb.getValue() > maxNb) {
                maxNb = nb.getValue();
            }
        }

        maxNb += nbJ;

        switch (maxNb) {
            case 5:
                return 6L;
            case 4:
                return 5L;
            case 3:
                if (nbJ == 0) { // AAAKK or AAAKQ
                    return cards.size() == 2 ? 4L : 3L;
                } else if (nbJ == 1) { // JAAKK or JAAKQ
                    return cards.size() == 3 ? 4L : 3L;
                } 
                return 3L; // JJAKQ
            case 2:
                if (nbJ == 0) { //AAKKQ or AAKQT
                    return cards.size() == 3 ? 2L : 1L; 
                }
                return 1L; // JAKQT
            default:
                return 0L;
        }
    }
}
