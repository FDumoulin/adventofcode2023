package thirteen;

import java.util.ArrayList;
import java.util.List;

public class Pattern {
    
    List<String> lines;
    List<String> columns;

    public Pattern() {
        this.lines = new ArrayList<>();
        this.columns = new ArrayList<>();
    }

    public Pattern(Pattern origin, int x, int y) {
        char toReplace = origin.lines.get(x).charAt(y) == '.' ? '#' : '.';

        this.lines = new ArrayList<>(origin.lines);
        this.lines.set(x, this.lines.get(x).substring(0, y) 
            + toReplace + this.lines.get(x).substring(y + 1));

        this.columns = new ArrayList<>(origin.columns);
        this.columns.set(y, this.columns.get(y).substring(0, x) 
            + toReplace + this.columns.get(y).substring(x + 1));
    }

    public void addLine(String line) {
        lines.add(line);
        if (columns.isEmpty()) {
            for (int i = 0; i < line.length(); i++) {
                columns.add(line.substring(i, i + 1));
            }
            return;
        }
        for (int i = 0; i < line.length(); i++) {
            columns.set(i, columns.get(i) + line.substring(i, i + 1));
        }
    }

    public int findSmudge() {
        int originRes = this.findMirror();
        for (int i = 0; i < lines.size(); i++) {
            for (int j = 0; j < columns.size(); j++) {
                Pattern pattern = new Pattern(this, i, j);
                int res = pattern.findMirror(originRes);
                if (res != 0) {
                    return res;
                }
            }
        }
        return 0;
    }

    public int findMirror() {
        for (int i = 0; i + 1 < lines.size(); i++) {
            if (lines.get(i).equals(lines.get(i + 1)) && checkMirror(lines, i, i + 1)) {
                return (i + 1) * 100;
            }
        }
        for (int i = 0; i + 1 < columns.size(); i++) {
            if (columns.get(i).equals(columns.get(i + 1)) && checkMirror(columns, i, i + 1)) {
                return i + 1;
            }
        }
        return 0;
    }

    public int findMirror(int originRes) {
        for (int i = 0; i + 1 < lines.size(); i++) {
            if (lines.get(i).equals(lines.get(i + 1)) && checkMirror(lines, i, i + 1)
                && (i + 1) * 100 != originRes) {
                return (i + 1) * 100;
            }
        }
        for (int i = 0; i + 1 < columns.size(); i++) {
            if (columns.get(i).equals(columns.get(i + 1)) && checkMirror(columns, i, i + 1)
                && i + 1 != originRes) {
                return i + 1;
            }
        }
        return 0;
    }

    private boolean checkMirror(List<String> list, int topRow, int bottomRow) {
        int i = 0;
        while (topRow - i >= 0 && bottomRow + i < list.size()) {
            if (!list.get(topRow - i).equals(list.get(bottomRow + i))) {
                return false;
            }
            i++;
        }
        return true;
    }
}
