package thirteen;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import main.Base;

public class Thirteen extends Base {

    List<Pattern> patterns;
    
    public int execute() throws FileNotFoundException {
        initialize();
        while (sc.hasNextLine()) {
            getPattern();
        }

        int sum = 0;
        for (Pattern pattern : patterns) {
            sum += pattern.findSmudge();
        }

        return sum;
    }

    private void initialize() throws FileNotFoundException {
        super.initialize("13");

        patterns = new ArrayList<>();
    }

    public void getPattern() {
        Pattern pattern = new Pattern();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equals("")) {
                patterns.add(pattern);
                return;
            }
            pattern.addLine(line);
        }
        patterns.add(pattern);
    }
}
