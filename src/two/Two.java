package two;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.Base;

// objet nombre avec valeur debut longueur
// pour chaque objet nombre récupérer liste des positions adjacentes
// check si un symbole existe à ces positions
// osef des positions incorrectes, il n'y a juste pas de symbole dessus


public class Two extends Base {
    
    private List<Game> games = new ArrayList<>();

    public int execute() throws FileNotFoundException {
        initialize();

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            createGame(line);
        }

        int sum = 0;
        // part 1
        // for (Game game : games) {
        //     sum += game.isGameValid(12, 13, 14)
        //         ? game.getId() : 0;
        // }

        // return sum;

        // part 2
        for (Game game : games) {
            sum += game.getPower();
        }

        return sum;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("6");
    }

    private void createGame(String line) {
        Game game = new Game(Integer.valueOf(line.split(":")[0].split(" ")[1]));
        for (String pull : line.split(":")[1].split(";")) {
            for (String pullByColor : pull.split(",")) {
                String[] colorAndValue = pullByColor.trim().split(" ");
                game.updateMax(Integer.valueOf(colorAndValue[0]), colorAndValue[1]);
            }
        }
        games.add(game);
    }
}
