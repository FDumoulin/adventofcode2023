package two;

import java.util.HashMap;
import java.util.Map;

public class Game {
    
    int id;
    Map<String, Integer> maxByColor;

    public Game(int id) {
        this.id = id;
        maxByColor = new HashMap<>();
        maxByColor.put("red", 0);
        maxByColor.put("green", 0);
        maxByColor.put("blue", 0);
    }

    public int getId() {
        return this.id;
    }

    public void updateMax(int value, String color) {
        if (maxByColor.get(color) < value) {
            maxByColor.put(color, value);
        }
    }

    public boolean isGameValid(int maxRed, int maxGreen, int maxBlue) {
        return maxByColor.get("red") <= maxRed
            && maxByColor.get("green") <= maxGreen
            && maxByColor.get("blue") <= maxBlue;
    }

    public int getPower() {
        return maxByColor.get("red") * maxByColor.get("green") * maxByColor.get("blue");
    }
}
