package one;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import main.Base;

public class One extends Base {

    private Map<String, Integer> lettersToNumber = new HashMap<>();
    private int sum;

    public int execute() throws FileNotFoundException {
        initialize();

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            findAndAddDigits(line);
        }
        return sum;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("1");

        lettersToNumber.put("one", 1);
        lettersToNumber.put("two", 2);
        lettersToNumber.put("three", 3);
        lettersToNumber.put("four", 4);
        lettersToNumber.put("five", 5);
        lettersToNumber.put("six", 6);
        lettersToNumber.put("seven", 7);
        lettersToNumber.put("eight", 8);
        lettersToNumber.put("nine", 9);

        sum = 0;
    }
    
    private void findAndAddDigits(String line) {

        int firstDigit = -1;
        int lastDigit = -1;

        for (int i=0; i < line.length(); i++) {
            char c = line.charAt(i);
            int val;

            if (Character.isDigit(c)) {
                val = Character.getNumericValue(c);
            } else {
                val = lookForNumberInLetters(line, i);
            }

            if (val != -1) {
                lastDigit = val;
                if (firstDigit == -1) {
                    firstDigit = val;
                }
            }
        }

        sum += firstDigit * 10 + lastDigit;
    }

    private Integer lookForNumberInLetters(String line, int pos) {
        for (Map.Entry<String, Integer> entry : lettersToNumber.entrySet()) {
            if(line.startsWith(entry.getKey(), pos)) {
                return entry.getValue();
            }
        }
        return -1;
    }
}