package four;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import main.Base;

public class Four extends Base {
    
    private List<Integer> winningNumbers;
    private Map<Integer, Integer> cards;
    
    public int execute() throws FileNotFoundException {
        initialize();

        int sum = 0;
        // part 1
        // while (sc.hasNextLine()) {
        //     sum += getNumbersPartOne(sc.nextLine());
        // }

        // part 2
        int i = 1;
        while (sc.hasNextLine()) {
            sum += getNumbersPartTwo(sc.nextLine(), i);
            i++;
        }

        return sum;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("4");

        winningNumbers = new ArrayList<>();
        cards = new HashMap<>();

        int i = 1;
        try (Scanner scCount = new Scanner(file)) {
            while (scCount.hasNextLine()) {
                cards.put(i, 1);
                scCount.nextLine();
                i++;
            }
        }
    }

    private int getNumbersPartOne(String line) {
        line = line.replace('|', 'b');
        for (String nb : line.split("b")[0].split(":")[1].trim().split("\\s+")) {
            winningNumbers.add(Integer.valueOf(nb));
        }

        int score = 0;
        for (String nb : line.split("b")[1].trim().split("\\s+")) {
            Integer cardNumber = Integer.parseInt(nb);
            if (winningNumbers.contains(cardNumber)) {
                score = score != 0 ? score * 2 : 1;
            }
        }
        
        winningNumbers.clear();
        return score;
    }

    private int getNumbersPartTwo(String line, int i) {
        line = line.replace('|', 'b');
        for (String nb : line.split("b")[0].split(":")[1].trim().split("\\s+")) {
            winningNumbers.add(Integer.valueOf(nb));
        }

        int winCount = 0;
        for (String nb : line.split("b")[1].trim().split("\\s+")) {
            Integer cardNumber = Integer.parseInt(nb);
            if (winningNumbers.contains(cardNumber) && i + winCount < cards.size()) {
                winCount++;
                cards.put(i + winCount, cards.get(i + winCount) + cards.get(i));
            }
        }

        winningNumbers.clear();
        return cards.get(i);
    }
}
