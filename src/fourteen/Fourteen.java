package fourteen;

import java.io.FileNotFoundException;
import java.sql.Time;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import main.Base;

public class Fourteen extends Base {
    
    public enum Rock { NO_ROCK, ROUND_ROCK, SQUARE_ROCK }
    List<List<Rock>> rocks;
    
    public int execute() throws FileNotFoundException {
        initialize();
        Instant i = Instant.now();
        for (long l = 0; l < 1000000; l++) {
            tilt();
        }
        System.out.println(i.until(Instant.now(), ChronoUnit.MILLIS));
        return 1;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("14");
        
        rocks = new ArrayList<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            List<Rock> row = new ArrayList<>();
            for (char c : line.toCharArray()) {
                if (c == '#') {
                    row.add(Rock.SQUARE_ROCK);
                } else if (c == 'O') {
                    row.add(Rock.ROUND_ROCK);
                } else {
                    row.add(Rock.NO_ROCK);
                }
            }
            rocks.add(row);
        }
    }

    private int tilt() {
        int sum = 0;
        for (int x = 0; x < rocks.size(); x++) {
            for (int y = 0; y < rocks.get(x).size(); y++) {
                if (get(x, y).equals(Rock.ROUND_ROCK)) {
                    sum += moveRock(x, y);
                }
            }
        }
        return sum;
    }

    private int moveRock(int x, int y) {
        while (x > 0 && get(x - 1, y).equals(Rock.NO_ROCK)) {
            set(x, y, Rock.NO_ROCK);
            set(x - 1, y, Rock.ROUND_ROCK);
            x--;
        }
        return rocks.size() - x;
    }

    // private int getWeight() {
    //     int sum = 0;
    //     for (int x = 0; x < rocks.size(); x++) {
    //         for (int y = 0; y < rocks.get(x).size(); y++) {
    //             if (get(x, y).equals(Rock.ROUND_ROCK))
    //         }
    //     }
    //     return sum;
    // }

    private Rock get(int x, int y) {
        return rocks.get(x).get(y);
    }

    private void set(int x, int y, Rock r) {
        rocks.get(x).set(y, r);
    }
}
