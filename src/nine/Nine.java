package nine;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import main.Base;

public class Nine extends Base {
    
    public long execute() throws FileNotFoundException {
        initialize();

        int sum = 0;
        while (sc.hasNextLine()) {
            List<Integer> history = new ArrayList<>();
            for (String s : sc.nextLine().split(" ")) {
                history.add(Integer.parseInt(s));
            }
            sum += getReversePrediction(history);
        }

        return sum;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("9");
    }

    public int getPrediction(List<Integer> history) {
        List<Integer> subHistory = new ArrayList<>();
        boolean noChange = true;
        for (int i = 0; i + 1 < history.size(); i++) {
            int val = history.get(i + 1) - history.get(i);
            subHistory.add(val);
            if (val != 0) {
                noChange = false;
            }
        }
        return history.get(history.size() - 1) + (noChange ? 0 : getPrediction(subHistory));
    }

    public int getReversePrediction(List<Integer> history) {
        List<Integer> subHistory = new ArrayList<>();
        boolean noChange = true;
        for (int i = 0; i + 1 < history.size(); i++) {
            int val = history.get(i + 1) - history.get(i);
            subHistory.add(val);
            if (val != 0) {
                noChange = false;
            }
        }
        return history.get(0) - (noChange ? 0 : getReversePrediction(subHistory));
    }
}
