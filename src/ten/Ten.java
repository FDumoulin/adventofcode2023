package ten;

import java.io.FileNotFoundException;

import main.Base;
import ten.PipeMap.Direction;

public class Ten extends Base {
    
    PipeMap map;
    
    public int execute() throws FileNotFoundException {
        initialize();

        map = new PipeMap();
        int x = 0;
        int y = 0;
        int xStart = -1;
        int yStart = -1;
        while (sc.hasNextLine()) {
            map.addRow();
            for (char c : sc.nextLine().toCharArray()) {
                if (c == 'S') {
                    xStart = x;
                    yStart = y;
                }
                map.add(c);
                y++;
            }
            x++;
            y = 0;
        }

        // part 1
        Direction d = null;
        x = xStart;
        y = yStart;
        int i = 0;

        while (x != xStart || y != yStart || i == 0) {
            d = map.get(x, y, d);
            switch (d) {
                case UP: x--; break;
                case DOWN: x++; break;
                case LEFT: y--; break;
                case RIGHT: y++; break;
            }
            i++;
        }

        // part 2
        // always going "counter clockwise"
        d = Direction.RIGHT;
        x = xStart;
        y = yStart;
        i = 0;

        while (x != xStart || y != yStart || i == 0) {
            d = map.get(x, y, d);
            switch (d) {
                case UP: x--; break;
                case DOWN: x++; break;
                case LEFT: y--; break;
                case RIGHT: y++; break;
            }
            map.checkOutsideDirection(x, y, d);
            i++;
        }

        return map.getNbEnclosed();
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("10");
    }
}