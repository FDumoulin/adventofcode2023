package ten;

import java.util.ArrayList;
import java.util.List;

public class PipeMap {

    public enum Direction { UP, DOWN, LEFT, RIGHT }
    
    List<List<Character>> pipes;
    List<List<Boolean>> isInTheLoop;
    List<List<Boolean>> isOutside;
    int nbOutside = 0;

    public PipeMap() {
        pipes = new ArrayList<>();
        isInTheLoop = new ArrayList<>();
        isOutside = new ArrayList<>();
    }

    public void addRow() {
        pipes.add(new ArrayList<>());
        isOutside.add(new ArrayList<>());
        isInTheLoop.add(new ArrayList<>());
    }

    public void add(Character c) {
        pipes.get(pipes.size() - 1).add(c);
        isInTheLoop.get(isInTheLoop.size() - 1).add(Boolean.FALSE);
        isOutside.get(isOutside.size() - 1).add(Boolean.FALSE);
    }

    public Direction get(int x, int y, Direction d) {
        isInTheLoop.get(x).set(y, Boolean.TRUE);
        switch (pipes.get(x).get(y)) {
            case '|':
            case '-':
                return d;
            case 'L':
                return d.equals(Direction.DOWN) ? Direction.RIGHT : Direction.UP;
            case 'J':
                return d.equals(Direction.DOWN) ? Direction.LEFT : Direction.UP;
            case '7':
                return d.equals(Direction.UP) ? Direction.LEFT : Direction.DOWN;
            case 'F':
                return d.equals(Direction.UP) ? Direction.RIGHT : Direction.DOWN;
            case 'S':
                return getStartDirection(x, y);
            default:
                return null;
        }
    }

    private Direction getStartDirection(int x, int y) {
        char c = pipes.get(x - 1).get(y);
        if (c == '|' || c == '7' || c == 'F') {
            return Direction.UP;
        }
        c = pipes.get(x + 1).get(y);
        if (c == '|' || c == 'J' || c == 'L') {
            return Direction.DOWN;
        }
        c = pipes.get(x).get(y - 1);
        if (c == '-' || c == 'L' || c == 'F') {
            return Direction.RIGHT;
        }
        c = pipes.get(x + 1).get(y);
        if (c == '-' || c == '7' || c == 'J') {
            return Direction.LEFT;
        }
        return null;
    }

    public void checkOutsideDirection(int x, int y, Direction dIncoming) {
        switch(pipes.get(x).get(y)) {
            case '|':
                if (dIncoming.equals(Direction.DOWN)) {
                    checkIfOutside(x, y - 1);
                } else {
                    checkIfOutside(x, y + 1);
                }
                break;
            case '-':
                if (dIncoming.equals(Direction.RIGHT)) {
                    checkIfOutside(x + 1, y );
                } else {
                    checkIfOutside(x - 1, y);
                }
                break;
            case 'L':
                if (dIncoming.equals(Direction.DOWN)) {
                    checkIfOutside(x, y - 1);
                    checkIfOutside(x + 1, y);
                }
                break;
            case 'J':
                if (dIncoming.equals(Direction.RIGHT)) {
                    checkIfOutside(x, y + 1);
                    checkIfOutside(x + 1, y);
                }
                break;
            case '7':
                if (dIncoming.equals(Direction.UP)) {
                    checkIfOutside(x, y + 1);
                    checkIfOutside(x - 1, y);
                }
                break;
            case 'F':
                if (dIncoming.equals(Direction.LEFT)) {
                    checkIfOutside(x, y - 1);
                    checkIfOutside(x - 1, y);
                }
                break;
            default:
                break;
        }
    }

    private void checkIfOutside(int x, int y) {
        if (x == -1 || y == -1 || x == pipes.size() || y == pipes.get(x).size()) {
            return;
        }

        if (getOutside(x, y) || getInTheLoop(x, y)) {
            return;
        }
        
        nbOutside++;
        setOutside(x, y, true);
        checkIfOutside(x - 1, y);
        checkIfOutside(x + 1, y);
        checkIfOutside(x, y - 1);
        checkIfOutside(x, y + 1);
    }

    private boolean getInTheLoop(int x, int y) {
        return isInTheLoop.get(x).get(y);
    }

    private boolean getOutside(int x, int y) {
        return isOutside.get(x).get(y);
    }

    private void setOutside(int x, int y, boolean b) {
        isOutside.get(x).set(y, b);
    }

    public void showTheLoop() {
        for (int x = 0; x < pipes.size(); x++) {
            for (int y = 0; y < pipes.get(x).size(); y++) {
                System.out.print(isInTheLoop.get(x).get(y).equals(Boolean.TRUE) 
                    ? pipes.get(x).get(y).toString() : " ");
            }           
            System.out.println();
        }
    }

    public int getNbEnclosed() {
        int i = 0;
        for (int x = 0; x < pipes.size(); x++) {
            for (int y = 0; y < pipes.get(x).size(); y++) {
                if (!getInTheLoop(x, y) && !getOutside(x, y)) {
                    System.out.print("X");
                    i++;
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        return i;
    }
}


// ELEVEN
// Faire la liste des positions des galaxies
// Pour chaque ligne
    // si aucune galaxie n'a l comme valeur de x
        // toutes les galaxies qui ont un x supérieur à l font x++
// Pour chaque colonne
    // si aucune galaxie n'a c comme valeur de y
        // toutes les galaxies qui ont un y supérieur à c font y++

// la distance entre chaque paire est égale à abs(x1 - x2) + abs(y1 - y2)