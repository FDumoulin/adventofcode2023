package five;

import java.util.ArrayList;
import java.util.List;

public class Mapping {
 
    private List<Long> start;
    private List<Long> end;
    private List<Long> step;

    public Mapping() {
        this.start = new ArrayList<>();
        this.end = new ArrayList<>();
        this.step = new ArrayList<>();
    }

    public void addMapping(long source, long destination, long length) {
        this.start.add(source);
        this.end.add(source + length);
        this.step.add(destination - source);
    }

    public long getMappedValue(long source) {
        for (int i = 0; i < this.start.size(); i++) {
            if (source >= this.start.get(i) && source <= this.end.get(i)) {
                return source + this.step.get(i);
            }
        }
        return source;
    }
}
