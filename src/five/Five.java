package five;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import main.Base;

public class Five extends Base {
    
    private List<Long> seedsOne;
    private Map<Long, Long> seedsTwo;
    private List<Mapping> mappings;
    
    public long execute() throws FileNotFoundException {
        initialize();

        String firstLine = sc.nextLine();
        for (String seed : firstLine.split(":")[1].trim().split(" ")) {
            seedsOne.add(Long.valueOf(seed));
        }

        Scanner lsc = new Scanner(firstLine);
        lsc.next();
        while (lsc.hasNext()) {
            seedsTwo.put(lsc.nextLong(), lsc.nextLong());
        }
        lsc.close();

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (!line.isEmpty()) {
                getMapping();
            }
        }

        long min = Long.MAX_VALUE;
        // part 1
        // for (Long seed : seedsOne) {
        //     long currentValue = seed;
        //     for (Mapping m : mappings) {
        //         currentValue = m.getMappedValue(currentValue);
        //     }
        //     if (currentValue < min) {
        //         min = currentValue;
        //     }
        // }

        // part 2
        for (Map.Entry<Long, Long> seedRange : seedsTwo.entrySet()) {
            for (int i = 0; i < seedRange.getValue(); i++) {
                long currentValue = seedRange.getKey() + i;
                for (Mapping m : mappings) {
                    currentValue = m.getMappedValue(currentValue);
                }
                if (currentValue < min) {
                    min = currentValue;
                }
            }
        }

        return min;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("5");

        seedsOne = new ArrayList<>();
        seedsTwo = new HashMap<>();
        mappings = new ArrayList<>();
    }

    private void getMapping() {
        Mapping mapping = new Mapping();
        String line = sc.nextLine();
        do {
            String[] splitLine = line.split(" ");
            mapping.addMapping(
                Long.valueOf(splitLine[1]),
                Long.valueOf(splitLine[0]),
                Long.valueOf(splitLine[2]));
            line = sc.nextLine();
        } while (sc.hasNextLine() && !line.isEmpty() && Character.isDigit(line.charAt(0)));
        mappings.add(mapping);
    }
}
