package twelve;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecordV2 {
    
    private String model;
    private List<Integer> damSequences;
    private Set<String> solutions;
    long nbValid = 0;

    public RecordV2(String line) {
        model = line.split(" ")[0];
        
        damSequences = new ArrayList<>();
        for (String s : line.split(" ")[1].split(",")) {
            damSequences.add(Integer.parseInt(s));
        }
    }

    public RecordV2 unfold() {
        System.out.println(model);
        StringBuilder newModel = new StringBuilder(model);
        List<Integer> newDamSequences = new ArrayList<>(damSequences);
        for (int i = 0; i < 4; i++) {
            newModel.append("?" + model);
            newDamSequences.addAll(damSequences);
        }
        model = newModel.toString();
        damSequences = newDamSequences;
        return this;
    }

    public long computeAllSolutions() {
        int additionalOp = model.length() - getSumDamaged() - damSequences.size() + 1;
        List<Integer> possibility = new ArrayList<>();
        possibility.add(0);
        for (int i = 0; i < damSequences.size() - 1; i++) {
            possibility.add(1);
        }
        possibility.add(0);

        nbValid = 0;
        solutions = new HashSet<>();
        generateAllPossibilities(new ArrayList<>(), additionalOp);
        return nbValid;
    }

    private int getSumDamaged() {
        int sum = 0;
        for (Integer i : damSequences) {
            sum += i;
        }
        return sum;
    }

    private void generateAllPossibilities(List<Integer> list, int remaining) {
        if (list.size() == damSequences.size() || remaining == 0) {
            list.add(remaining);
            for (int i = list.size(); i < damSequences.size() + 1; i++) {
                list.add(0);
            }

            String solution = getSolutionFromSequences(list);
            if (checkAgainstModel(solution)) {
                 nbValid++;
                // solutions.add(solution);
            }
            return;
        }

        for (int i = 0; i <= remaining; i++) {
            List<Integer> newList = new ArrayList<>(list);
            newList.add(i);
            String preSolution = getSolutionFromSequences(list);
            if (checkAgainstModel(preSolution)) {
                generateAllPossibilities(newList, remaining - i);
            }
        }
    }

    private String getSolutionFromSequences(List<Integer> opSequences) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < opSequences.size(); i++) {
            int opMax = opSequences.get(i) 
                + (i != 0 && i != damSequences.size() ? 1 : 0);
            for (int j = 0; j < opMax; j++) {
                sb.append(".");
            }
            if (i < damSequences.size()) {
                for (int j = 0; j < damSequences.get(i); j++) {
                    sb.append("#");
                }
            }
        }
        return sb.toString();
    }

    private boolean checkAgainstModel(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (model.charAt(i) != '?' && model.charAt(i) != str.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
