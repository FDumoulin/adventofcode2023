package twelve;

import java.util.ArrayList;
import java.util.List;

public class Record {
    
    public enum Gear { UNK, OP, DAM }

    List<Gear> gearStates;
    List<Integer> damagedSequences;
    int nbValid;

    public Record(String line) {
        gearStates = new ArrayList<>();
        for (char c : line.split(" ")[0].toCharArray()) {
            if (c == '?') {
                gearStates.add(Gear.UNK);
            } else if (c == '.') {
                gearStates.add(Gear.OP);
            } else if (c == '#') {
                gearStates.add(Gear.DAM);
            }
        }

        damagedSequences = new ArrayList<>();
        for (String s : line.split(" ")[1].split(",")) {
            damagedSequences.add(Integer.parseInt(s));
        }
    }

    public int getSize() {
        return gearStates.size();
    }

    private boolean isValid(List<Gear> g) {
        int s = -1;
        int cpt = 0;
        Gear lastState = Gear.OP;
        for (int i = 0; i <g.size(); i++) {
            if (g.get(i).equals(Gear.DAM)) {
                if (lastState.equals(Gear.OP)) {
                    s++;
                    cpt = 1;
                    // too many sequences
                    if (s >= damagedSequences.size()) {
                        return false;
                    }
                } else if (lastState.equals(Gear.DAM)) {
                    cpt++;
                    // too many damaged in one sequence
                    if (cpt > damagedSequences.get(s)) {
                        return false;
                    }
                }
            // not enough damaged in one sequence
            } else if (lastState.equals(Gear.DAM) && cpt != damagedSequences.get(s)) {
                return false;
            }
            lastState = g.get(i);
        }

        // not enough sequences or not enough damaged in one sequence
        return s == damagedSequences.size() - 1 && cpt == damagedSequences.get(s);
    }

    private boolean canBeValid(List<Gear> g) {
        int s = -1;
        int cpt = 0;
        Gear lastState = Gear.OP;
        for (int i = 0; i <g.size(); i++) {
            if (g.get(i).equals(Gear.UNK)) {
                return true;
            }
            if (g.get(i).equals(Gear.DAM)) {
                if (lastState.equals(Gear.OP)) {
                    s++;
                    cpt = 1;
                    // too many sequences
                    if (s >= damagedSequences.size()) {
                        return false;
                    }
                } else if (lastState.equals(Gear.DAM)) {
                    cpt++;
                    // too many damaged in one sequence
                    if (cpt > damagedSequences.get(s)) {
                        return false;
                    }
                }
            // not enough damaged in one sequence
            } else if (lastState.equals(Gear.DAM) && cpt != damagedSequences.get(s)) {
                return false;
            }
            lastState = g.get(i);
        }

        // if we go till the end it doesnt matter
        return true;
    }

    public int checkAllPossibilities() {
        nbValid = 0;
        checkAllPossibilities(gearStates);
        return nbValid;
    }

    private void checkAllPossibilities(List<Gear> incomplete) {
        for (int i = 0; i < incomplete.size(); i++) {
            if (incomplete.get(i).equals(Gear.UNK)) {
                List<Gear> list1 = new ArrayList<>(incomplete);
                list1.set(i, Gear.DAM);
                checkAllPossibilities(list1);

                List<Gear> list2 = new ArrayList<>(incomplete);
                list2.set(i, Gear.OP);
                checkAllPossibilities(list2);
                return;
            }
        }

        if (isValid(incomplete)) {
            nbValid++;
        }
    }

    public int checkSomePossibilities() {
        nbValid = 0;
        checkSomePossibilites(gearStates);
        return nbValid;
    }

    private void checkSomePossibilites(List<Gear> incomplete) {
        if (!canBeValid(incomplete)) {
            return;
        }

        for (int i = 0; i < incomplete.size(); i++) {
            if (incomplete.get(i).equals(Gear.UNK)) {
                List<Gear> list1 = new ArrayList<>(incomplete);
                list1.set(i, Gear.DAM);
                checkSomePossibilites(list1);

                List<Gear> list2 = new ArrayList<>(incomplete);
                list2.set(i, Gear.OP);
                checkSomePossibilites(list2);
                return;
            }
        }

        if (isValid(incomplete)) {
            nbValid++;
        }
    }

    public Record unfold() {
        List<Gear> newGearStates = new ArrayList<>();
        List<Integer> newDamagedSequences = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            newGearStates.addAll(gearStates);
            newDamagedSequences.addAll(damagedSequences);
            if (i < 4) {
                newGearStates.add(Gear.UNK);
            }
        }
        gearStates = newGearStates;
        damagedSequences = newDamagedSequences;
        return this;
    }
}
