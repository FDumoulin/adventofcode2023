package twelve;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import main.Base;

public class Twelve extends Base {

    List<RecordV2> records;
    
    public long execute() throws FileNotFoundException {
        initialize();

        while (sc.hasNextLine()) {
            records.add(new RecordV2(sc.nextLine()));
        }

        long sum = 0;
        for (RecordV2 r : records) {
            // part 1
            // sum += r.computeAllSolutions();
            // part 2
            sum += r.unfold().computeAllSolutions();
            System.out.println(sum);
        }

        return sum;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("12");

        records = new ArrayList<>();
    }
}
