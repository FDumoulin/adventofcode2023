package eight;

public class Node {
    
    private String left;
    private String right;

    public Node(String left, String right) {
        this.left = left;
        this.right = right;
    }

    public String getLeft() {
        return this.left;
    }

    public String getRight() {
        return this.right;
    }
}
