package eight;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.Base;

public class Eight extends Base {

    Map<String, Node> nodes;
    List<String> startLocations;
    List<Long> loopPeriods;
    
    public long execute() throws FileNotFoundException {
        initialize();

        char[] directions = sc.nextLine().toCharArray();

        sc.nextLine();
        while (sc.hasNextLine()) {
            getNode(sc.nextLine());
        }

        // part 1
        // int i = 0;
        // String location = "AAA";
        // while (!location.equals("ZZZ")) {
        //     location = directions[i%directions.length] == 'L'
        //         ? nodes.get(location).getLeft()
        //         : nodes.get(location).getRight();
        //     i++;
        // }
        
        // part 2
        for (int l = 0; l < startLocations.size(); l++) {
            String location = startLocations.get(l);
            int i = 0;
            while (!location.substring(2, 3).equals("Z")) {
                location = directions[i%directions.length] == 'L'
                    ? nodes.get(location).getLeft()
                    : nodes.get(location).getRight();
                i++;
            }
            loopPeriods.add(Long.valueOf(i));
        }

        Long incr = loopPeriods.get(0);
        Long result = incr;
        for (int l = 0; l + 1 < loopPeriods.size(); l++) {
            incr = result;
            while (result % loopPeriods.get(l + 1) != 0) {
                result += incr;
            }
        }

        return result;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("8");

        nodes = new HashMap<>();
        startLocations = new ArrayList<>();
        loopPeriods = new ArrayList<>();
    }

    private void getNode(String line) {
        if (line.substring(2, 3).equals("A")) {
            startLocations.add(line.substring(0, 3));
        }
        nodes.put(
            line.substring(0, 3),
            new Node(line.substring(7, 10), line.substring(12, 15)));
    }
}
