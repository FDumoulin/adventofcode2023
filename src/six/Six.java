package six;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import main.Base;

public class Six extends Base {

    public long execute() throws FileNotFoundException {
        initialize();

        // part 1
        // Map<Long, Long> races = readPartOne();

        // part 2
        Map<Long, Long> races = readPartTwo();

        long sumButForLikeMultiplication = 1;
        for (Map.Entry<Long, Long> race : races.entrySet()) {
            long maxTime = race.getKey();
            long distanceRecord = race.getValue();

            long holdTime = 0;
            long distance = 0;
            while (distance <= distanceRecord) {
                holdTime++;
                distance = holdTime * (maxTime - holdTime);
            }

            sumButForLikeMultiplication *= maxTime - 2 * holdTime + 1;
        }

        return sumButForLikeMultiplication;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("6");
    }

    private Map<Long, Long> readPartOne() {
        Scanner scl1 = new Scanner(sc.nextLine());
        Scanner scl2 = new Scanner(sc.nextLine());

        scl1.next();
        scl2.next();

        Map<Long, Long> races = new HashMap<>();
        while (scl1.hasNext()) {
            races.put(scl1.nextLong(), scl2.nextLong());
        }

        scl1.close();
        scl2.close();

        return races;
    }

    private Map<Long, Long> readPartTwo() {
        Map<Long, Long> races = new HashMap<>();
        races.put(
            Long.valueOf(sc.nextLine().split(":")[1].trim().replace(" ", "")),
            Long.valueOf(sc.nextLine().split(":")[1].trim().replace(" ", "")));
        return races;
    }
}
