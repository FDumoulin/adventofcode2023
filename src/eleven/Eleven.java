package eleven;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import main.Base;
import three.Position;

public class Eleven extends Base {

    List<Position> galaxies;
    int x = 0;
    int y = 0;

    // part 1
    // int expansionFactor = 1;

    // part 1
    int expansionFactor = 999999;
    
    public long execute() throws FileNotFoundException {
        initialize();

        x = 0;
        y = 0;

        getGalaxies();
        expandLines();
        expandColumns();

        long sum = 0;
        for (int i = 0; i + 1 < galaxies.size(); i++) {
            for (int j = i + 1; j < galaxies.size(); j++) {
                sum+= getDistance(galaxies.get(i), galaxies.get(j));
            }
        }

        // printGalaxies();
        return sum;
    }
    
    private void initialize() throws FileNotFoundException {
        super.initialize("11");

        galaxies = new ArrayList<>();
    }

    private void getGalaxies() {
        while (sc.hasNextLine()) {
            y = 0;
            for (char c : sc.nextLine().toCharArray()) {
                if (c == '#') {
                    galaxies.add(new Position(x, y));
                }
                y++;
            }
            x++;
        }
    }

    @SuppressWarnings("java:S127")
    private void expandLines() {
        for (int l = 0; l < x; l++) {
            boolean shouldExpand = true;
            for (Position galaxy : galaxies) {
                if (galaxy.getX() == l) {
                    shouldExpand = false;
                }
            }
            
            if (shouldExpand) {
                for (Position galaxy : galaxies) {
                    if (galaxy.getX() > l) {
                        galaxy.setX(galaxy.getX() + expansionFactor);
                    }
                }
                l += expansionFactor;
                x += expansionFactor;
            }
        }
    }

    @SuppressWarnings("java:S127")
    private void expandColumns() {
        for (int c = 0; c < y; c++) {
            boolean shouldExpand = true;
            for (Position galaxy : galaxies) {
                if (galaxy.getY() == c) {
                    shouldExpand = false;
                }
            }
            
            if (shouldExpand) {
                for (Position galaxy : galaxies) {
                    if (galaxy.getY() > c) {
                        galaxy.setY(galaxy.getY() + expansionFactor);
                    }
                }
                c += expansionFactor;
                y += expansionFactor;
            }
        }
    }

    private int getDistance(Position galaxy1, Position galaxy2) {
        return Math.abs(galaxy1.getX() - galaxy2.getX())
            + Math.abs(galaxy1.getY() - galaxy2.getY());
    }

    private void printGalaxies() {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                    boolean isThere = false;
                for (Position p : galaxies) {
                    if (p.getX() == i && p.getY() == j) {
                        isThere = true;
                    }
                }
                System.out.print(isThere ? "#" : ".");
            }
            System.out.println();
        }
    }
}
