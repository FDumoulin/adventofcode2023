package three;

public class Number {
    
    private int value;
    private Position start;
    private int length;

    public Number(int value, int x, int y, int length) {
        this.value = value;
        this.start = new Position(x, y);
        this.length = length;
    }

    public int getValue() {
        return this.value;
    }

    public boolean isAdjacentToSymbol(Position pos) {
        return pos.getX() >= start.getX() - 1 && pos.getX() <= start.getX() + 1
            && pos.getY() >= start.getY() - 1 && pos.getY() <= start.getY() + length;
    }
}
