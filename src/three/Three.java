package three;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.Base;

public class Three extends Base {
    
    List<Number> numbers;
    List<Position> symbols;
    List<Position> gears;
    
    public int execute() throws FileNotFoundException {
        initialize();
        createNumbersAndSymbols();

        int sum = 0;
        // part 1
        // for (Number number : numbers) {
        //     if (anySymbolIsAdjacent(number)) {
        //         System.out.println(number.getValue());
        //         sum += number.getValue();
        //     }
        // }

        // part 2
        for (Position gear : gears) {
            sum += findGearNumbers(gear);
        }
        return sum;
    }

    @SuppressWarnings("java:S1075")
    private void initialize() throws FileNotFoundException {
        super.initialize("3");

        numbers = new ArrayList<>();
        symbols = new ArrayList<>();
        gears = new ArrayList<>();
    }

    private void createNumbersAndSymbols() {
        int x = 0;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            for (int y = 0; y < line.length(); y++) {
                char c = line.charAt(y);
                if (Character.isDigit(c)) {
                    numbers.add(getNumber(line, x, y));
                    do { // skip to the next non digit
                        y++;
                    } while (y < line.length()-1 && Character.isDigit(line.charAt(y + 1)));
                } else if (c != '.') {
                    symbols.add(new Position(x, y));
                        if (c == '*') {
                        gears.add(new Position(x, y));
                    }
                }
            }
            x++;
        }
    }

    private Number getNumber(String line, int x, int y) {
        StringBuilder sb = new StringBuilder();
        int length = 0;
        char c = line.charAt(y + length);
        while (Character.isDigit(c)) {
            sb.append(c);
            length++;
            c = y + length < line.length() ? line.charAt(y + length) : '.';
        }
        return new Number(Integer.valueOf(sb.toString()), x, y, length);
    }

    private boolean anySymbolIsAdjacent(Number number) {
        for (Position symbol : symbols) {
            if (number.isAdjacentToSymbol(symbol)) {
                return true;
            }
        }
        return false;
    }

    private int findGearNumbers(Position gear) {
        int gearValue1 = 0;
        int gearValue2 = 0;

        for (Number number : numbers) {
            if (number.isAdjacentToSymbol(gear)) {
                if (gearValue1 == 0) {
                    gearValue1 = number.getValue();
                } else {
                    gearValue2 = number.getValue();
                }
            }
        }
        return gearValue1 * gearValue2;
    }
}
