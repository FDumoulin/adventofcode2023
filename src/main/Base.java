package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public abstract class Base {
    
    protected Scanner sc;
    protected File file;

    protected void initialize(String filename) throws FileNotFoundException {
        file = new File("C:/Users/flodu/OneDrive/Documents/AdventOfCode/resources/" + filename + ".txt");
        sc = new Scanner(file);
    }
}
