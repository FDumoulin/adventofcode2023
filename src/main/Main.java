package main;

import java.io.FileNotFoundException;

import fourteen.Fourteen;

public class Main {
    
    @SuppressWarnings("java:S106")
    public static void main(String[] args) throws FileNotFoundException {
        Fourteen number = new Fourteen();
        System.out.println(number.execute());
    }
}
